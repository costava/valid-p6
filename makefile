.PHONY: test clean

test: bin/test
	./bin/test

bin:
	mkdir bin

bin/test: IsValidRestrictedP6.c test/test.c | bin
	gcc -std=c99 -o $@ -g -I ./ test/test.c

clean:
	rm -f ./bin/test
