#include <stdbool.h>
#include <stdint.h>

/*@ predicate P_IsWhitespace(uint8_t c) =
	c == ' '  || c == '\r' || c == '\n' ||
	c == '\t' || c == 11   || c == 12;
*/

// Return whether the given ASCII code point is whitespace
//  according to the PPM spec.
/*@
	assigns \nothing;
	ensures \result <==> P_IsWhitespace(c);
*/
static bool IsWhitespace(uint8_t c) {
	// 11 is vertical tab (VT).
	// 12 is form feed (FF).
	return
		c == ' '  || c == '\r' || c == '\n' ||
		c == '\t' || c == 11   || c == 12;
}

/*@
	assigns \nothing;
	ensures \result == (d >= '0' && d <= '9');
*/
static bool IsDigit(uint8_t d) {
	return d >= '0' && d <= '9';
}

// Leading 0's are permitted as long the number
//  is 5 digits or fewer.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires 0 <= bufLen <= 5;
	requires \forall uint8_t i; (0 <= i < bufLen) ==>
		('0' <= buf[i] <= '9');
	assigns \nothing;
*/
static uint16_t ParseDecimalU16(
	const uint8_t *const buf,
	const uint8_t bufLen)
{
	uint16_t result = 0;

	uint8_t i = 0;

	/*@
		loop invariant 0 <= i <= bufLen;
		loop assigns i, result;
		loop variant bufLen - i;
	*/
	while (i < bufLen) {
		result *= 10;
		result += buf[i] - '0';
		i += 1;
	}

	return result;
}

// Return the lowest index that is >= start
//  where buf[that index] is not a digit.
// That index is the bufLen if the remainder of
//  the buf is digits.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires start <= bufLen;
	assigns \nothing;
	ensures start <= \result <= bufLen;
	ensures \forall uint64_t i; (start <= i < \result) ==>
		('0' <= buf[i] <= '9');
*/
static uint64_t FindNatEndExc(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start)
{
	uint64_t result = start;

	/*@
		loop invariant start <= result <= bufLen;
		loop invariant \forall uint64_t i; (start <= i < result) ==>
			('0' <= buf[i] <= '9');
		loop assigns result;
		loop variant bufLen - result;
	*/
	while (result < bufLen && IsDigit(buf[result])) {
		result += 1;
	}

	return result;
}

// Return the lowest index that is >= start
//  where buf[that index] is not whitespace.
// That index is the bufLen if the remainder of
//  the buf is whitespace.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires start <= bufLen;
	assigns \nothing;
	ensures start <= \result <= bufLen;
	ensures \forall uint64_t i; (start <= i < \result) ==>
		P_IsWhitespace(buf[i]);
*/
static uint64_t FindWhitespaceEndExc(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start)
{
	uint64_t result = start;

	/*@
		loop invariant start <= result <= bufLen;
		loop invariant \forall uint64_t i; (start <= i < result) ==>
			P_IsWhitespace(buf[i]);
		loop assigns result;
		loop variant bufLen - result;
	*/
	while (result < bufLen && IsWhitespace(buf[result])) {
		result += 1;
	}

	return result;
}

// Consume whitespace, then non-final decimal U16.
// "Non-final" meaning the last digit of the U16
//   is NOT the last character of the buf.
// Returns true iff successful.
/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	requires \valid(out_value);
	requires \valid(out_nextIndex);
	requires start <= bufLen;
	assigns *out_value, *out_nextIndex;
	ensures \result ==> *out_nextIndex < bufLen;
*/
static bool ConsumeWNFDU16(
	const uint8_t *const buf,
	const uint64_t bufLen,
	const uint64_t start,
	uint16_t *const out_value,
	uint64_t *const out_nextIndex)
{
	const uint64_t firstIndex = FindWhitespaceEndExc(buf, bufLen, start);

	if (firstIndex == bufLen) {
		return false;
	}

	/*@ assert firstIndex < bufLen; */

	if (!IsDigit(buf[firstIndex])) {
		return false;
	}

	/*@ assert '0' <= buf[firstIndex] <= '9'; */

	const uint64_t endIndexExc = FindNatEndExc(buf, bufLen, firstIndex + 1);

	if (endIndexExc == bufLen) {
		return false;
	}

	/*@ assert firstIndex < endIndexExc < bufLen; */

	const uint64_t numDigits = endIndexExc - firstIndex;

	if (numDigits > 5) {
		return false;
	}

	//@ assert 1 <= numDigits <= 5;
	//@ assert (firstIndex + numDigits) < bufLen;
	/*@ assert \forall uint64_t i; (firstIndex <= i < (firstIndex + numDigits)) ==>
		('0' <= buf[i] <= '9');
	*/

	*out_value = ParseDecimalU16(&buf[firstIndex], numDigits);
	*out_nextIndex = endIndexExc;

	return true;
}

/*@
	requires \valid_read(&buf[0 .. bufLen-1]);
	assigns \nothing;
*/
bool IsValidRestrictedP6(
	const uint8_t *const buf,
	const uint64_t bufLen)
{
	// Minimum length is 9.
	// "P6 0 0 1 "
	if (bufLen < 9) {
		return false;
	}

	if (buf[0] != 'P' || buf[1] != '6' || !IsWhitespace(buf[2])) {
		return false;
	}

	uint16_t width;
	uint16_t height;
	uint16_t maxValue;
	uint64_t nextIndex;

	{
		// Take advantage of short-circuit evaluation.
		const bool success =
			ConsumeWNFDU16(buf, bufLen, 3,         &width,    &nextIndex) &&
			ConsumeWNFDU16(buf, bufLen, nextIndex, &height,   &nextIndex) &&
			ConsumeWNFDU16(buf, bufLen, nextIndex, &maxValue, &nextIndex);

		if (!success) {
			return false;
		}
	}

	//@ assert nextIndex < bufLen;

	if (maxValue == 0) {
		return false;
	}

	if (!IsWhitespace(buf[nextIndex])) {
		return false;
	}

	const uint64_t bytesPerChannel = (maxValue >= 256) ? 2 : 1;
	const uint64_t expectedNumComponentBytes = (uint64_t)width * height * bytesPerChannel * 3;

	//@ assert (bytesPerChannel == 2) ==> (expectedNumComponentBytes % 2) == 0;

	//@ assert nextIndex < bufLen;

	// Spec requires exactly 1 character of whitespace
	// between the max value and the image data.
	// Here, we step past that character.
	const uint64_t firstComponentIndex = nextIndex + 1;
	const uint64_t numComponentBytes = bufLen - firstComponentIndex;

	if (numComponentBytes != expectedNumComponentBytes) {
		return false;
	}

	//@ assert firstComponentIndex <= bufLen;
	//@ assert (bytesPerChannel == 2) ==> (numComponentBytes % 2) == 0;

	if (bytesPerChannel == 1) {
		/*@
			loop invariant firstComponentIndex <= i <= bufLen;
			loop assigns i;
			loop variant bufLen - i;
		*/
		for (uint64_t i = firstComponentIndex; i < bufLen; i += 1) {
			if (buf[i] > maxValue) {
				return false;
			}
		}
	}
	else {
		//@ assert bytesPerChannel == 2;

		//@ assert (firstComponentIndex + numComponentBytes) == bufLen;
		//@ assert (numComponentBytes % 2) == 0;
		//@ assert ((bufLen - firstComponentIndex) % 2) == 0;

		/*@
			loop invariant firstComponentIndex <= i <= bufLen;
			loop invariant ((bufLen - i) % 2) == 0;
			loop assigns i;
			loop variant bufLen - i;
		*/
		for (uint64_t i = firstComponentIndex; i < bufLen; i += 2) {
			// Spec requires that most significant byte comes first.
			const uint16_t value = buf[i] * UINT16_C(256) + buf[i + 1];

			//@ assert (buf[i] > 0) ==> (value >= 256);

			if (value > maxValue) {
				return false;
			}
		}
	}

	return true;
}
