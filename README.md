# Valid P6

`IsValidRestrictedP6` is a function that returns whether the given buffer is a valid P6 PPM image,
with some restrictions not present in the spec.

The specification of a P6 PPM is given by [this netpbm document](https://netpbm.sourceforge.net/doc/ppm.html)

The above document specifies that:
> A PPM file consists of a sequence of one or more PPM images. There are no data, delimiters, or padding before, after, or between images.

This repo has a restriction that the given buffer must be exactly 1 image.

The spec does not specify a limit on the width and height,
but this repo requires that the width and height both be less than 65536.

The spec does not specify whether leading 0's are permitted.
This repo permits leading 0's, but the total number of digits in the number must be <= 5.

This repo does NOT permit comments in the buffer/file.

## Annotations

The functions are annotated with the ANSI/ISO C Specification Language (ACSL).
The annotations can be verified with a command like `frama-c -wp -wp-rte IsValidRestrictedP6.c`

All 174 goals are verified successfully.

Tested with:
- frama-c 25.0 (Manganese)
- alt-ergo 2.4.0
