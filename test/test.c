#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// Not ideal but works
#include "IsValidRestrictedP6.c"

// Reads entire file at given path into given buf and populates given length var.
// On success, *out_fileBuf is pointer to null-terminated string.
// out_fileLen does not include the null terminator.
// On success: *out_fileBuf is from malloc
// On error:
// - Prints to stderr
// - calls exit(1)
/*@
  requires valid_read_string(path);
  requires \valid(out_fileLen);
  requires \separated(path, out_fileLen);

  assigns out_fileBuf[0];
  assigns out_fileLen[0];

  ensures 0 <= *out_fileLen <= SIZE_MAX;
  ensures 0 <= *out_fileLen <= UINT64_MAX;
*/
void io_ReadEntireFile(
	const char *const path,
	uint8_t* out_fileBuf[const static 1],
	uint64_t out_fileLen[const static 1])
{
	FILE *const f = fopen(path, "rb");

	if (f == NULL) {
		fprintf(stderr, "%s: Error calling fopen on %s\n", __func__, path);
		exit(1);
	}

	if (fseek(f, 0, SEEK_END) != 0) {
		fprintf(stderr, "%s: Failed to fseek to end of FILE* of %s\n", __func__, path);
		fclose(f);
		exit(1);
	}

	const long fileLen = ftell(f);

	if (fileLen == -1L) {
		fprintf(stderr, "%s: ftell error on FILE* of %s\n", __func__, path);
		perror("ftell");
		fclose(f);
		exit(1);
	}

	assert(fileLen >= 0);
	_Static_assert(LONG_MAX <= SIZE_MAX, "Runtime check needed");

	const size_t fileLenS = fileLen;

	if (fileLenS == SIZE_MAX) {
		fprintf(stderr, "%s: File %s is too long (%zu bytes)\n", __func__, path, SIZE_MAX);
		fclose(f);
		exit(1);
	}

	const size_t bufSize = fileLenS + 1;
	char *const buf = malloc(bufSize);

	if (buf == NULL) {
		fprintf(stderr,
			"%s: Error calling malloc with %zu for holding the contents of %s\n",
			__func__, bufSize, path);

		fclose(f);
		exit(1);
	}

	//@ assert \valid(&buf[0 .. bufSize-1]);

	if (fileLen > 0) {
		rewind(f);

		if (fread(buf, (size_t)fileLen, 1, f) != 1) {
			fprintf(stderr, "%s: fread error on FILE* of %s\n", __func__, path);

			fclose(f);
			free(buf);
			exit(1);
		}
	}

	if (fclose(f) != 0) {
		fprintf(stderr, "%s: fclose error on FILE* of %s\n", __func__, path);

		free(buf);
		exit(1);
	}

	buf[fileLen] = '\0';

	_Static_assert(LONG_MAX <= UINT64_MAX, "Runtime check needed");
	_Static_assert(CHAR_BIT == 8, "char type required to have 8 bits");

	*out_fileBuf = (uint8_t*)buf;
	*out_fileLen = (uint64_t)fileLen;
}

typedef struct TCase {
	uint8_t *path;
	bool expectedResult;
} TCase;

int main(void) {
	TCase cases[] = {
		{"test/only_p6.ppm", false}, // Incomplete.
		{"test/stripe.ppm", true},
		{"test/stripe_max_16.ppm", false}, // Values are greater than max.
		{"test/stripe_with_comment.ppm", false} // Comments not permitted.
	};

	const uint64_t numCases = sizeof(cases) / sizeof(cases[0]);

	for (uint64_t c = 0; c < numCases; c += 1) {
		uint8_t *fileBuf;
		uint64_t fileLen;

		io_ReadEntireFile(cases[c].path, &fileBuf, &fileLen);

		assert(IsValidRestrictedP6(fileBuf, fileLen) == cases[c].expectedResult);

		free(fileBuf);
	}

	return 0;
}
